package com.example.android.labintroandroid;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by Jose Tejada on 22/2/2018.
 */

public class Listado implements Parcelable {
    protected String nameList;
    protected ArrayList<Alumno> AlumnList;

    public  Listado(){
        nameList="";
       AlumnList= new ArrayList<>();
       Alumno a1= new Alumno();
       AlumnList.add(a1);
    }

    protected Listado(Parcel in) {
        nameList = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(nameList);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Listado> CREATOR = new Creator<Listado>() {
        @Override
        public Listado createFromParcel(Parcel in) {
            return new Listado(in);
        }

        @Override
        public Listado[] newArray(int size) {
            return new Listado[size];
        }
    };
}
