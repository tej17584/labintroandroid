package com.example.android.labintroandroid;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    //Creamos una variable tipo Listview y otra de vector para llenarla
    private ListView list;
    private String[] comidas;

    public MainActivity() {
        comidas = new String[]{"Guacamole","Nachos","Mojarron Perez","Pizza","McDonald's","Whopper","Subway","Picnic","Gitane","Shucazo"};
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //Buscamos con el ID al ListView de la vista
        list=(ListView) findViewById(R.id.listViewComidas);
        //Ahora usaremos el Adatper
        ArrayAdapter<String> adpatador= new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,comidas);
        list.setAdapter(adpatador);
        //Ahora creamos un evento por elemento de la Lista
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent;
                String valor;

                valor = (String) list.getItemAtPosition(position);
                intent = new Intent(view.getContext(),Main2Activity.class);
                intent.putExtra("Comidita",valor);
                startActivity(intent);
            }
        });

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,Main3Activity.class);
                Listado allList= new Listado();
                intent.putExtra(Main3Activity.SELECTED_ITEM,allList);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
