package com.example.android.labintroandroid;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Jose Tejada on 22/2/2018.
 */

public class Alumno implements Parcelable{
    protected int age;
    protected String name;
    protected String carnet;

    public Alumno(){
        age=0;
        name="";
        carnet="";
    }

    protected Alumno(Parcel in) {
        age = in.readInt();
        name = in.readString();
        carnet = in.readString();
    }

    public static final Creator<Alumno> CREATOR = new Creator<Alumno>() {
        @Override
        public Alumno createFromParcel(Parcel in) {
            return new Alumno(in);
        }

        @Override
        public Alumno[] newArray(int size) {
            return new Alumno[size];
        }
    };

    public String getName(){
        return this.name;
    }

    public String getCarnet(){
        return this.carnet;
    }

    public int getAge(){
        return this.age;
    }

    public void setName(String name){
        this.name=name;
    }
    public void setAge(int Age){
        this.age=age;
    }

    public void setCarnet(String carnet){
        this.carnet=carnet;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(age);
        dest.writeString(name);
        dest.writeString(carnet);
    }
}
